import unittest
import atlann


class TestMethods(unittest.TestCase):

    def test_read_config(self):

        config = atlann.read_configs('project-input.json')
        self.assertEqual(config['databases']['A']['connection-ip'], 'localhost')
        self.assertEqual(config['csv_path'], 'test.csv')
        self.assertEqual(config['contact']['contact-value'], 'shahin@atlann.co')

    def test_read_csv(self):

        csv = atlann.read_csv('test.csv')
        self.assertEqual(csv[0]['id'], '1')
        self.assertEqual(csv[0]['name'], 'Edward')
