import csv
import json
import smtplib
import ssl
import logging

from patabase import Mssql
from patabase import Postgres


def read_configs(config_file: str) -> dict:
    with open(config_file) as json_file:
        input_json = json.load(json_file)

        return {
            'databases': input_json['databases'][0],
            'csv_path': input_json['csv-files']['full-path'],
            'contact': input_json['contact-info']
        }


def read_csv(csv_file_path: str) -> list:
    with open(csv_file_path, encoding='utf-8') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        rows = list(csv_reader)
        return rows


def send_email(receiver_email, message: str) -> None:
    port = 465
    smtp_server = "smtp.gmail.com"
    sender_email = input("Type your email address here: ")
    password = input("Type your password and press enter: ")

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email, message)


def transport(config: dict) -> None:
    db1 = Postgres(host=config['databases']['A']['connection-ip'], user=config['databases']['A']['username'],
                   password=config['databases']['A']['password'], database=config['databases']['A']['db-name'])

    db2 = Mssql(host=config['databases']['B']['connection-ip'], user=config['databases']['B']['username'],
                password=config['databases']['B']['password'], database=config['databases']['B']['db-name'])

    records = read_csv(config['csv_path'])
    for record in records:
        db1.perform(f'''
            insert into {config['databases']['A']['table-name']}(id, name, family, age) values(%s, %s, %s, %s); 
            ''', record['id'], record['name'], record['family'], record['age'])

        db2.perform(f'''
             insert into {config['databases']['B']['table-name']}(id, name, family, age) values(%s, %s, %s, %s);
            ''', record['id'], record['name'], record['family'], record['age'])

    send_email(receiver_email=config['contact']['contact-value'], message='All data transferred successfully!')


def main() -> None:
    cnf_file = 'project-input.json'
    try:
        cnf = read_configs(cnf_file)
        transport(cnf)
    except Exception as e:
        logging.log(0, str(e))


if __name__ == '__main__':
    main()
